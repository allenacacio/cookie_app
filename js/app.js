function setCookie(elem) {
    var expirationDays = 30;
    var cookieName = elem.name;
    var cookieValue = elem.value; 
    var d = new Date();
    d.setTime(d.getTime() + (expirationDays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cookieName + "=" + cookieValue + "; " + expires;
}

function getCookie(cookieName) {
    var name = cookieName + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) {
		  return c.substring(name.length, c.length);	
		}
    }
    return "";
}

function submitForm(btnElem){
    var formName = "myForm";
    var form = document.forms[formName];
    var page_redirect = "";
    var cls = btnElem.getAttribute("class");

    if (cls.indexOf("btn-info")>-1){//Learn more is clicked...
        page_redirect = "route-landing.html";
    } else if (cls.indexOf("btn-danger")>-1) {//Get a quote is clicked...
        page_redirect = "request-quote.html";
    }
    form.action = page_redirect;
    form.submit();
}

function display_message(type,elemMsgId){
      var page_message = "";
      var cookieFrom = getCookie("cookieFrom");
      var cookieTo = getCookie("cookieTo");
	  if (cookieFrom!=="" && cookieTo!==""){
		  page_message = "<h1>"+ type +" Detail in Cookie is: <br/>";
		  page_message = page_message + " From: " + cookieFrom + "<br/>";
		  page_message = page_message + " To: " + cookieTo + "<br/></h1>";
	  }else{
		  page_message = "<h2>There is no cookie stored in browser. <br/> Please go to main page by clicking the link below.</h2>";
	  }
	  document.getElementById("page_message").innerHTML = page_message;	
}

function init(){
	var fromElem = document.getElementsByName("cookieFrom");
	var toElem = document.getElementsByName("cookieTo");
	setCookie(fromElem[0]);
	setCookie(toElem[0]);
}
